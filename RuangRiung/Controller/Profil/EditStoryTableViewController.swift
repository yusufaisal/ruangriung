//
//  AddStoryTableViewController.swift
//  RuangRiung
//
//  Created by Louis  Valen on 23/08/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit
import MapKit
import SwiftyDropbox
class EditStoryTableViewController: UITableViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, MKMapViewDelegate, CLLocationManagerDelegate,TypeDelegate {
    
    func type(datas: [Bool]) {
        statusType = datas
    }
    @IBOutlet weak var showNameSwitch: UISwitch!
    @IBOutlet weak var titleText: UITextField!
    
    @IBOutlet weak var contentText: UITextView!
    
    @IBOutlet weak var attachPhotoButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    let regionInMeters: Double = 1000
    var currentLocation: CLLocationCoordinate2D? // var yang di pake
    var post:Post?
    var postUrls:[String]?
    var images:[UIImage] = [UIImage]()
    var loading:Int = 0
    var loaded:Int = 0
    var collection:UICollectionView!
    let imageWidth = 150
    let imageHeight = 150
    let buttonSize = 25
    var editedImage = false
    @IBOutlet weak var viewForCollectionView: UIView!
    
    @IBOutlet weak var DetailLabel: UILabel!
    var statusType:[Bool] = [false,false,false,false,false]
    //ganti dong jadi yg punya post
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "types"{
            let statustype = segue.destination as! TypeTableViewController
            statustype.type = sender as? [Bool]
            statustype.delegate = self
        }
    }
    
    
    @IBAction func TypeButton(_ sender: Any) {
         performSegue(withIdentifier: "types", sender: statusType)
    }
    
    
    func TypeLabel() -> String{
        var temp = ""
        if statusType[0] == true{
            temp += "Innapropriate Touching. "
        }
        if statusType[1] == true{
            temp.append("Cat Calling. ")
        }
        if statusType[2] == true{
            temp.append("Stalking. ")
        }
        if statusType[3] == true{
            temp.append("Racism. ")
        }
        if statusType[4] == true{
            temp.append("Other.")
        }
        return temp
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DetailLabel.text = TypeLabel()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        initLabels()
        
        initPictures()
        mapView.delegate = self
        checkLocationServices()
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.9376266599, green: 0.3580080867, blue: 0.360183239, alpha: 1)

    }
    func initLabels(){
        self.contentText.text = post?.postContent
        self.titleText.text = post?.title
        if post?.name?.lowercased() == "anonymous"{
            self.showNameSwitch.isOn = false
        }else{
            self.showNameSwitch.isOn = true
        }
        if let types = post?.types{
            if types.count >= 5{
                statusType = types
            }
        }
    }
    func initPictures(){
        postUrls = (post?.urls ?? [String]())
        loading = ((postUrls?.count ?? 0))
        initCollection()
        if let checkUrls = postUrls{
            for pUrl in checkUrls{
                let url = URL(string: pUrl)!
                downloadImage(from: url)
                //downloadImg(url: url)
            }
        }
    }
    @IBAction func doneButtonClicked(_ sender: Any) {
        SavePost()
        //loading bar
        performSegue(withIdentifier: "editToProfile", sender: self)
    }
    
    @IBAction func addPhotoButtonClicked(_ sender: Any) {
        if images.count < 3{
            ImagePickerManager().pickImage(self){ image in
                self.images.append(image)
                self.editedImage = true
                self.collection.reloadData()
                self.checkImagesCount()
            }
        }
    }
    
    func SavePost(){
        let ck = CloudKitHelper()
        var tPost:Post
        let title = self.titleText.text ?? ""
        let postContent = self.contentText.text ?? ""
        let types = ""
        var name = "Anonymous"
        if showNameSwitch.isOn{
            var locked = true
            ck.getUser(){
                user,error in
                name = user?.name ?? "Anonymous"
                locked = false
            }
            while(locked){sleep(1)}
        }
        let lat = currentLocation?.latitude ?? 0
        let long = currentLocation?.longitude ?? 0
        let urls = post?.urls ?? [String]()
        tPost = Post(location: CLLocation(latitude: lat, longitude: long), title: title, postContent: postContent, dateCreated: Date(), dateLastModified: Date(), name: name, urls: urls, types: (post?.types ?? [Bool]()),commentCount: 0)
        tPost.recordName = post?.recordName
        if images.count > 0 && editedImage{
            let imageNames:[String] = getImageName(imageList: images)
            uploadImg(post:tPost,imageNames: imageNames, images: images)
        }else{
            //let imgs:[CKAsset] = uploadImage(imageList: images)
            ck.editPost(post: tPost)
        }
    }
    
    func getImageName(imageList:[UIImage])->[String]{
        var returnURLS:[String] = [String]()
        for imageToUpload in imageList{
            if let checkAsset = getImageLocation(myImage: imageToUpload){
                returnURLS.append(checkAsset)
            }
        }
        return returnURLS
    }
    func uploadImg(post:Post,imageNames: [String], images: [UIImage]){
        var urls:[String] = [String]()
        let counter = imageNames.count
        for(index,_) in imageNames.enumerated(){
            self.uploadImage(imageName: imageNames[index], image: images[index]){
                url,_ in
                urls.append(url)
                self.uploadImgBG(post: post, urls: urls,totalImg: counter,currentImg: index)
            }
        }
    }
    func uploadImgBG(post:Post,urls:[String],totalImg:Int,currentImg:Int){
        if totalImg == (currentImg+1){
            var ePost = post
            ePost.urls = urls
            let ck = CloudKitHelper()
            ck.editPost(post: ePost)
        }
    }
    @IBAction func CancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 5
    }
    //
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            self.setImg(data: data)
        }
    }
    func setImg(data:Data){
        DispatchQueue.main.async {
            if let img = UIImage(data: data){
                self.images.append(img)
            }
            self.loaded += 1
            self.collection.reloadData()
            self.checkImagesCount()
        }
    }
    func collectionView(_ collection: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if images.count > 3{
            while(images.count > 3){
                let _ = images.popLast()
            }
        }
        return images.count
    }
    func collectionView(_ collection: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        let tempimage = images[indexPath.row]
        let imageView = UIImageView(image: tempimage)
        imageView.frame = CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight);
        cell.addSubview(imageView)
        let button = UIButton(frame: CGRect(x: (imageWidth-buttonSize), y: 0, width: buttonSize, height: buttonSize))
        button.backgroundColor = .red
        button.setTitle("x", for: .normal)
        switch indexPath.row{
        case 0:
            button.addTarget(self, action: #selector(buttonAction0), for: .touchUpInside)
            cell.addSubview(button)
            break
        case 1:
            button.addTarget(self, action: #selector(buttonAction1), for: .touchUpInside)
            cell.addSubview(button)
            break
        case 2:
            button.addTarget(self, action: #selector(buttonAction2), for: .touchUpInside)
            cell.addSubview(button)
            break
        default:
            print("error")
            break
        }
        return cell
    }
    //brute force
    @objc func buttonAction0(sender: UIButton!) {
        images.remove(at: 0)
        self.collection.reloadData()
        checkImagesCount()
    }
    @objc func buttonAction1(sender: UIButton!) {
        images.remove(at: 1)
        self.collection.reloadData()
        checkImagesCount()
    }
    @objc func buttonAction2(sender: UIButton!) {
        images.remove(at: 2)
        self.collection.reloadData()
        checkImagesCount()
    }
    func checkImagesCount(){
        print("loading ",loading,"     loaded ",loaded)
        if ((images.count < 3)&&(loading <= loaded)){
            self.attachPhotoButton.isEnabled = true
            self.attachPhotoButton.setTitleColor(UIColor(cgColor: #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)), for: UIControl.State.normal)
        }else{
            self.attachPhotoButton.isEnabled = false
            self.attachPhotoButton.setTitleColor(UIColor(cgColor: #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)), for: UIControl.State.disabled)
        }
    }
    func collectionView(_ collection: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: imageWidth, height: imageHeight)
        return size
    }
    func initCollection(){
        viewForCollectionView.frame =  CGRect(x: 0 , y: 0, width: self.view.frame.width, height: CGFloat(imageHeight))
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = CGSize(width: imageWidth, height: imageHeight)
        layout.scrollDirection = .horizontal
        collection = UICollectionView.init(frame: self.viewForCollectionView.frame, collectionViewLayout: layout)
        collection.dataSource = self
        collection.delegate = self
        collection.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        collection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        self.viewForCollectionView.addSubview(collection)
    }
    // coding map
    func checkLocationServices(){
        if CLLocationManager.locationServicesEnabled(){
            // setup our location manager
            setupLoactionManager()
            checkLocationAuthorization()
        } else {
            print("is disabled")
        }
    }
    
    func setupLoactionManager(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    func centerViewLocation(){
        if let location = post?.location.coordinate{
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
        }
    }
    func checkLocationAuthorization(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            mapView.showsUserLocation = true
            mapView.showsCompass = true
            mapView.showsPointsOfInterest = true
            locationManager.startUpdatingLocation()
            centerViewLocation()
            
        case .authorizedAlways:
            mapView.showsUserLocation = true
            mapView.showsCompass = true
            mapView.showsPointsOfInterest = true
            locationManager.startUpdatingLocation()
            centerViewLocation()
        case .denied:
            break
        case .restricted:
            break
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
        @unknown default:
            fatalError()
        }
    }
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        self.currentLocation = mapView.centerCoordinate
        print(currentLocation.debugDescription)
    }
    let client = DropboxClient(accessToken: DropboxAPI_AccessToken)
    func uploadImage(imageName:String,image:UIImage,complete: @escaping (_ instance: String, _ error: NSError?) -> ()){
        DispatchQueue.main.async {
            if let img = image.jpegData(compressionQuality: 0.2){
                let fileData:Data = img
                let fileName:String = imageName
                
                let request = self.client.files.upload(path: BASE_LOCATION+fileName, input: fileData)
                    .response { response, error in
                        if let response = response {
                            let request = self.client.sharing.createSharedLinkWithSettings(path: response.pathDisplay!, settings: Sharing.SharedLinkSettings(requestedVisibility: .public_, linkPassword: nil, expires: nil, audience: nil, access: nil)).response(completionHandler: { (res, err) in
                                if let res = res {
                                    var fixedUrl = (res.url.trimmingCharacters(in: CharacterSet(charactersIn: "dl=0 "))+"&raw=1")
                                    complete(fixedUrl,nil)
                                }
                            })
                            
                        }
                    }
                    .progress { progressData in
                        print(progressData)
                }
            }
        }
    }
    
    private func turnTextViewPlaceholder(bool: Bool){
        if bool {
            contentText.text = "Tell us what happened..."
            contentText.textColor = .lightGray
        } else {
            contentText.text = ""
            contentText.textColor = .black
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.textColor == .lightGray){
            turnTextViewPlaceholder(bool: false)
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text == ""){
            turnTextViewPlaceholder(bool: true)
            textView.resignFirstResponder()
        }
    }
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        switch section {
        case 2:
            let label = UILabel()
            label.frame = CGRect(x: 20, y: 8, width: 320, height: 20)
            label.font = UIFont.systemFont(ofSize: 12)
            label.textColor = #colorLiteral(red: 0, green: 0, blue: 0.1921568627, alpha: 0.6)
            label.text = self.tableView(tableView, titleForFooterInSection: section)
            
            let footer = UIView()
            footer.backgroundColor = #colorLiteral(red: 0.9685532451, green: 0.9686692357, blue: 0.968513906, alpha: 1)
            footer.addSubview(label)
            
            return footer
        default:
            return UIView()
        }
    }
}
