//
//  ChangeNameViewController.swift
//  RuangRiung
//
//  Created by Louis  Valen on 27/08/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit

class ChangeNameViewController: UIViewController {

    @IBOutlet weak var changeNameTextField: UITextField!
    var nama: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        changeNameTextField.clearButtonMode = UITextField.ViewMode.whileEditing
        changeNameTextField.text = nama
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.9376266599, green: 0.3580080867, blue: 0.360183239, alpha: 1)
        // Do any additional setup after loading the view.
    }
    @IBAction func saveButtonClicked(_ sender: Any) {
        let ck = CloudKitHelper()
        let name = self.changeNameTextField.text ?? ""
        ck.getUser(){
            user, _ in
            if user != nil{
                var checkUser = user!
                checkUser.name = name
                ck.editUser(user: checkUser)
            }
        }
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
