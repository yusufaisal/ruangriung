//
//  TypeTableViewController.swift
//  RuangRiung
//
//  Created by Louis  Valen on 23/08/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit
protocol TypeDelegate {
    func type(datas: [Bool])
}

class TypeTableViewController: UITableViewController {
    var type:[Bool]?
    
    var delegate: TypeDelegate? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.937254902, green: 0.3568627451, blue: 0.3607843137, alpha: 1)
    }

    @IBAction func BackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
  
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 5
        }else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "typeee", for: indexPath)
        let cells = UITableViewCell()
        
        switch indexPath.section {
        case 0:
            switch indexPath.row{
            case 0:
                cell.textLabel?.text = "Innapropriate Touching"
                if type![0] == true{
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                return cell
            case 1:
                cell.textLabel?.text = "Cat Calling"
                if type![1] == true{
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                return cell
            case 2:
                cell.textLabel?.text = "Stalking"
                if type![2] == true{
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                return cell
            case 3:
                cell.textLabel?.text = "Racism"
                if type![3] == true{
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                return cell
            case 4:
                cell.textLabel?.text = "Other"
                if type![4] == true{
                    cell.accessoryType = .checkmark
                }else{
                    cell.accessoryType = .none
                }
                return cell
            default:
                return cells
            }
        default:
            return cells
        }
    }
    

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.dequeueReusableCell(withIdentifier: "typeee", for: indexPath)
        switch indexPath.section {
        case 0:
            switch indexPath.row{
            case 0:
                if type![0] == false{
                    cell.accessoryType = .checkmark
                    type![0] = true
                }else{
                    cell.accessoryType = .none
                    type![0] = false
                }
                print(type![0])
                delegate?.type(datas: type!)
                self.tableView.reloadData()
                tableView.deselectRow(at: IndexPath.init(row: indexPath.row, section: indexPath.section), animated: true)
                
                break
            case 1:
                if type![1] == false{
                    cell.accessoryType = .checkmark
                    type![1] = true
                }else{
                    cell.accessoryType = .none
                    type![1] = false
                }
                print(type![1])
                
                delegate?.type(datas: type!)
                self.tableView.reloadData()
                tableView.deselectRow(at: IndexPath.init(row: indexPath.row, section: indexPath.section), animated: true)
                break
            case 2:
                if type![2] == false{
                    cell.accessoryType = .checkmark
                    type![2] = true
                }else{
                    cell.accessoryType = .none
                    type![2] = false
                }
                print(type![2])
                delegate?.type(datas: type!)
                self.tableView.reloadData()
                tableView.deselectRow(at: IndexPath.init(row: indexPath.row, section: indexPath.section), animated: true)
                break
            case 3:
                if type![3] == false{
                    cell.accessoryType = .checkmark
                    type![3] = true
                }else{
                    cell.accessoryType = .none
                    type![3] = false
                }
                print(type![3])
                delegate?.type(datas: type!)
                self.tableView.reloadData()
                tableView.deselectRow(at: IndexPath.init(row: indexPath.row, section: indexPath.section), animated: true)
                break
            case 4:
                if type![4] == false{
                    cell.accessoryType = .checkmark
                    type![4] = true
                }else{
                    cell.accessoryType = .none
                    type![4] = false
                }
                print(type![4])
                delegate?.type(datas: type!)
                self.tableView.reloadData()
                tableView.deselectRow(at: IndexPath.init(row: indexPath.row, section: indexPath.section), animated: true)
                break
                
            
            default:
                break
            }
        default:
            break;
        }
    }
    
    
    
    
    

//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 2
//    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
