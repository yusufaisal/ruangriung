//
//  DetailPostController.swift
//  RuangRiung
//
//  Created by iSal on 18/08/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit


class DetailFeedController: UIViewController, UITableViewDelegate,UITableViewDataSource, DetailFeedProtocol {
    @IBOutlet weak var tabelView: UITableView!
    let APIHelper = CloudKitHelper()
    var refresher:UIRefreshControl!
    var posts:Post!
    var comments:[Comment] = [Comment]()
    var myComment: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabelView()
        getComments()
        
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.9376266599, green: 0.3580080867, blue: 0.360183239, alpha: 1)
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = UIColor.red
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.tabelView.addSubview(refresher)
    }
    
    @objc func loadData() {
        DispatchQueue.main.async {
            self.getPost()
            self.getComments()
            
            self.refresher.endRefreshing()
        }
    }
    
    private func getComments(){
        APIHelper.getComment(postRecordName: posts.recordName!) { (comments, err) in
            DispatchQueue.main.async {
                self.comments = comments
                
                self.tabelView.reloadData()
            }
        }
    }
    
    private func getPost(){
        APIHelper.getPost(recordName: posts.recordName ?? ""){(post, err) in
            DispatchQueue.main.async {
                if let p = post{
                    self.posts = p
                }
                
                self.tabelView.reloadData()
            }
        }
    }
    
    func setupTabelView(){
        self.tabelView.dataSource = self
        self.tabelView.register(UINib(nibName: MapCell.identifier, bundle: nil), forCellReuseIdentifier: MapCell.identifier)
        self.tabelView.register(UINib(nibName: HeaderCell.identifier, bundle: nil), forCellReuseIdentifier: HeaderCell.identifier)
        self.tabelView.register(UINib(nibName: CommentFormCell.identifier, bundle: nil), forCellReuseIdentifier: CommentFormCell.identifier)
        self.tabelView.register(UINib(nibName: CommentTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: CommentTableViewCell.identifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    func changeComment(newComment: String) {
        self.myComment = newComment
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4+comments.count
    }
    
    func convertToString(types: [Bool]) -> String {
        var arrString:[String] = []
        for (i, type)  in types.enumerated() {
            if type{
                arrString.append(stringOfType[i])
            }
        }
        
        let str:String = arrString.joined(separator: ", ")
        
        if str==""{
            return "Unknown Type"
        } else {
            return str
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tabelView.dequeueReusableCell(withIdentifier: MapCell.identifier) as? MapCell else {return UITableViewCell()}
            cell.centerViewLocation(location: posts.location.coordinate)
            
            return cell
        case 1:
            guard let cell = tabelView.dequeueReusableCell(withIdentifier: HeaderCell.identifier) as? HeaderCell else {return UITableViewCell()}
            cell.author.text = posts.name
            cell.title.text = posts.title
            cell.type.text = convertToString(types: posts.types)
            cell.date.text = dateToString(date:posts.dateCreated)
            
            let activityIndicator = UIActivityIndicatorView()
            activityIndicator.center = cell.mainVIew.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.style = .gray
            cell.mainVIew.addSubview(activityIndicator)
            
            if posts.urls.count==0{
                cell.setupMainView(isPhotoAvailable: false)
            } else {
                cell.setupMainView(isPhotoAvailable: true)
                activityIndicator.startAnimating()
                for (idx,url) in posts.urls.enumerated(){
                    
                    let pictureURL = URL(string:url)!
                    
                    let session = URLSession(configuration: .default)
                    
                    let downloadPicTask = session.dataTask(with: pictureURL) { (data, response, error) in
                        if let e = error {
                            print("Error downloading cat picture: \(e)")
                        } else {
                            if let _ = response as? HTTPURLResponse {
                                // print("Downloaded cat picture with response \(res.statusCode)")
                                DispatchQueue.main.async {
                                    if let imageData = data {
                                        let image =  UIImage(data: imageData)
                                        switch idx{
                                        case 0:
                                            cell.image1.image = image
                                            cell.image1.isHidden = false
                                            
                                            let gesture = UITapGestureRecognizer(target: self, action: #selector(self.imageTap))
                                            cell.image1.addGestureRecognizer(gesture)
                                            cell.image1.isUserInteractionEnabled = true
                                            
                                            activityIndicator.stopAnimating()
                                        case 1:
                                            cell.image2.image = image
                                            cell.image2.isHidden = false
                                            
                                            let gesture = UITapGestureRecognizer(target: self, action: #selector(self.imageTap))
                                            cell.image2.addGestureRecognizer(gesture)
                                            cell.image2.isUserInteractionEnabled = true
                                        case 2:
                                            cell.image3.image = image
                                            cell.image3.isHidden = false
                                            
                                            let gesture = UITapGestureRecognizer(target: self, action: #selector(self.imageTap))
                                            cell.image3.addGestureRecognizer(gesture)
                                            cell.image3.isUserInteractionEnabled = true
                                            
                                        default:
                                            break
                                        }
                                    } else {
                                        print("Couldn't get image: Image is nil")
                                    }
                                }
                            } else {
                                // print("Couldn't get response code for some reason")
                            }
                            
                        }
                    }
                    downloadPicTask.resume()
                }
            }
            
           
            return cell
        case 2:
            let cell = UITableViewCell()
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.font = .systemFont(ofSize: 17)
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 5
            
            let att = NSMutableAttributedString(string: posts.postContent)
            att.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, att.length))
            cell.textLabel?.attributedText = att
            
            return cell
        case 3:
            guard let cell = tabelView.dequeueReusableCell(withIdentifier: CommentFormCell.identifier) as? CommentFormCell else {return UITableViewCell()}
            cell.numOfComment.text = String(comments.count)
            cell.delegate = self
            return cell
        default:
            guard let cell = tabelView.dequeueReusableCell(withIdentifier: CommentTableViewCell.identifier) as? CommentTableViewCell else {return UITableViewCell()}
            cell.time.text = timeAgoSinceDate(comments[indexPath.row-4].dateCreated, currentDate: Date(), numericDates: true)
            cell.name.text = "Anon"
            cell.content.text = comments[indexPath.row-4].commentContent
            return cell
        }
    }
    
    
    func addComment(){
        DispatchQueue.main.async {
            let cm:Comment = Comment(commentContent: self.myComment, dateCreated: Date(), dateLastModified: Date(), post: nil, thumbsUp: 0, thumbsDown: 0)
            self.APIHelper.addComment(comment: cm, post: self.posts)
            self.getComments()
            self.getComments()
        }
        
    }
    
    @objc func imageTap(sender:UITapGestureRecognizer){
         if let image = sender.view as? UIImageView
        {
            let view = ViewImageController()
            view.image = image.image
            present(view, animated: true)
        }
    }
    
    @IBAction func onCloseTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
