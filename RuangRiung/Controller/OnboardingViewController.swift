//
//  OnboardingViewController.swift
//  RuangRiung
//
//  Created by Louis  Valen on 04/09/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController,UIScrollViewDelegate {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    var slides:[Slide] = [];
    
    @IBOutlet weak var pageControl: UIPageControl!
    override func viewDidLoad() {
        super.viewDidLoad()
    
        scrollView.delegate = self
        
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubviewToFront(pageControl)
    }
    
    func createSlides() -> [Slide] {
        
        let slide1:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide1.ImageView.image = UIImage(named: "OB1")
        slide1.TitleLabel.text = "Share Your Story"
        slide1.readyButton.isHidden = true
        
        let slide2:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide2.ImageView.image = UIImage(named: "OB2")
        slide2.TitleLabel.text = "Support each other"
        slide2.readyButton.isHidden = true
        
        let slide3:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide3.ImageView.image = UIImage(named: "OB3")
        slide3.TitleLabel.text = "Avoid being a victim"
        slide3.readyButton.isHidden = true
        
        let slide4:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide4.ImageView.image = UIImage(named: "OB4")
        slide4.TitleLabel.text = "Start Now"
        slide4.readyButton.addTarget(self, action: #selector(onTap), for: .touchDown)
        slide4.readyButton.isHidden = false
        
        return [slide1, slide2, slide3, slide4]
    }
    
    @objc func onTap(){
        let ck = CloudKitHelper()
        ck.initUser()
        self.performSegue(withIdentifier: "gotohome", sender: nil)
    }
    
    func setupSlideScrollView(slides : [Slide]) {
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.addSubview(slides[i])
        }
    }

    private func  getPage() -> Int{
        return Int(scrollView.contentOffset.x / scrollView.frame.size.width)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = getPage()
        pageControl.currentPage = page
    }
}
