//
//  ProfileTableViewController.swift
//  RuangRiung
//
//  Created by Louis  Valen on 23/08/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit
import MapKit
class ProfileTableViewController: UITableViewController {
    var refresher:UIRefreshControl!
    let ck:CloudKitHelper = CloudKitHelper()
    var userPosts:[Post]?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getData()
        self.navigationItem.title = "Profil"
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = UIColor.red
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.view.addSubview(refresher)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getData()
    }
    
    @IBAction func unwindToProfileTVC(segue:UIStoryboardSegue) { }
    @objc func loadData() {
        getData()
        self.refresher.endRefreshing()
    }
    // MARK: - Table view data source
    func getData(){
        ck.getUser(){
            user, error in
            if let name = user?.name{
                self.namaUser = name
                print(name)
                self.tableView.reloadData()
            }
        }
        ck.getPosts(){
            posts, error in
            self.userPosts = posts
            self.tableView.reloadData()
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1 {
            return (self.userPosts?.count ?? 0)
        }else if section == 2{
            return 2
        }else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Name"
        }else if section == 1 {
            return "Stories"
        }else if section == 2 {
            return " "
        }
        return ""
    }
    

   
    var namaUser:String = "Loading"
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profile", for: indexPath)
        let cells = UITableViewCell()
        switch indexPath.section {
        case 0:
            switch indexPath.row{
            case 0:
                cell.textLabel?.text = namaUser
                print("93",namaUser)
                cell.accessoryType = .disclosureIndicator
                return cell
            default:
                return cells
            }
        case 1:
            // tinggal kasi judul per orang post
            if let checkPosts = userPosts{
                cell.textLabel?.textColor = .black
                cell.textLabel?.text = checkPosts[indexPath.row].title
                cell.accessoryType = .disclosureIndicator
            }
            return cell
        case 2:
            switch indexPath.row{
            case 0:
                cell.textLabel?.textColor = .red
                cell.textLabel?.text = "Term of Use"
                cell.accessoryType = .disclosureIndicator
                return cell
                
            case 1:
                cell.textLabel?.textColor = .red
                cell.accessoryType = .disclosureIndicator
                cell.textLabel?.text = "Privacy Policy"
                return cell
            default:
                return cells
            }
        default:
            return cells
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "changename"{
            let destName = segue.destination as! ChangeNameViewController
            destName.nama = sender as! String
            
        }
        if segue.identifier == "editStory"{
            let destName = segue.destination as! EditStoryTableViewController
            destName.post = sender as! Post
        }
    }

 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                performSegue(withIdentifier: "changename", sender: namaUser )
                tableView.deselectRow(at: IndexPath.init(row: 0, section: 0), animated: true)
                
            }
        }
        if indexPath.section == 1{
            performSegue(withIdentifier: "editStory", sender: userPosts?[indexPath.row])
            tableView.deselectRow(at: IndexPath.init(row: indexPath.row, section: 1), animated: true)
        }
        if indexPath.section == 2{
            if indexPath.row == 0{
                performSegue(withIdentifier: "termofuse", sender: self)
                tableView.deselectRow(at: IndexPath.init(row: 0, section: 2), animated: true)
            }
        }
        if indexPath.section == 2{
            if indexPath.row == 1{
                performSegue(withIdentifier: "privacypolice", sender: self)
                tableView.deselectRow(at: IndexPath.init(row: 1, section: 2), animated: true)
            }
        }
        
        
        
        
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
