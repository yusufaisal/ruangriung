//
//  CommentTableViewCell.swift
//  RuangRiung
//
//  Created by iSal on 29/08/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {
    
    static let identifier:String  = "CommentTableViewCell"

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var content: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
