//
//  cloudkitHelper.swift
//  RuangRiung
//
//  Created by michael gunawan on 20/08/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import Foundation
import CloudKit
import MapKit

class CloudKitHelper{
    //add a post to cloudkit
    func addPost(post:Post){
        DispatchQueue.global().async {
            // Fetch Default Container
            let defaultContainer = CKContainer.default()
            // Fetch User Record
            defaultContainer.fetchUserRecordID { (recordID, error) -> Void in
                if let responseError = error {
                    print(responseError)
                } else if let userRecordID = recordID {
                    // Fetch Default Container
                    let defaultContainer = CKContainer.default()
                    // Fetch Private Database
                    let publicDatabase = defaultContainer.publicCloudDatabase
                    // Fetch User Record
                    publicDatabase.fetch(withRecordID: userRecordID) { (record, error) -> Void in
                        if let responseError = error {
                            print(responseError)
                        } else if let userRecord = record{
                            self.addPostBG(user: userRecord,post: post)
                        }
                    }
                }
            }
        }
    }
    
    //continuation of addpost()
    private func addPostBG(user:CKRecord,post:Post){
        DispatchQueue.global().async {
            let rec = CKRecord(recordType: "Post")
            rec["location"] = post.location
            rec["name"] = post.name
            rec["title"] = post.title
            rec["postContent"] = post.postContent
            rec["dateCreated"] = post.dateCreated
            rec["dateLastModified"] = post.dateLastModified
            //rec["type"] = post.type
            rec["owner"] = CKRecord.Reference(record: user, action: .deleteSelf)
            rec["urls"] = post.urls
            rec["types"] = post.types
            rec["commentCount"] = post.commentCount
            let publicData = CKContainer.default().publicCloudDatabase
            publicData.save(rec) { (record:CKRecord?, error:Error?) in
                if error == nil{
                    print("saved")
                }else{
                    print(error as Any)
                }
            }
        }
    }
    
    //get all post of current user. belum di sort by date
    func getPosts(complete: @escaping (_ instance: [Post], _ error: NSError?) -> ()){
        getCurrentUserIDAsync() {
            record, error in
            if let uID = record?.recordName {
                let reference = CKRecord.Reference(recordID: CKRecord.ID(recordName: uID), action: .deleteSelf)
                let pred = NSPredicate(format: "owner == %@", reference)
                let sort = NSSortDescriptor(key: "creationDate", ascending: false)
                let query = CKQuery(recordType: "Post", predicate: pred)
                query.sortDescriptors = [sort]
                CKContainer.default().publicCloudDatabase.perform(query, inZoneWith: nil) {[self] results, error in
                    if let error = error {
                        print(error.localizedDescription)
                        complete([Post](),error as NSError)
                    } else {
                        if let checkedResults = results {
                            let posts = self.parsePostResults(records: checkedResults)
                            complete(posts,nil)
                        }
                    }
                }
            } else {
                print("Fetched iCloudID was nil")
            }
        }
    }
    
    //get all post of current user. belum di sort by date
    func getPost(recordName:String, complete: @escaping (_ instance: Post?, _ error: NSError?) -> ()){
        let pred = NSPredicate(format: "recordName == %@", recordName)
        let sort = NSSortDescriptor(key: "creationDate", ascending: false)
        let query = CKQuery(recordType: "Post", predicate: pred)
        query.sortDescriptors = [sort]
        CKContainer.default().publicCloudDatabase.perform(query, inZoneWith: nil) {[self] results, error in
            if let error = error {
                print(error.localizedDescription)
                complete(nil,error as NSError)
            } else {
                if let checkedResults = results {
                    let post = self.parsePostResults(records: checkedResults).first
                    complete(post,nil)
                }
            }
        }

    }
    
    private func parsePostResults(records: [CKRecord])->[Post] {
        var posts = [Post]()
        
        for record in records {
            posts.append(Post(recordName: record.recordID.recordName,location: record["location"]!, title: record["title"]!, postContent: record["postContent"]!, dateCreated: record["dateCreated"]!, dateLastModified: record["dateLastModified"]!, owner: record["owner"],name: (record["name"] ?? "Anonymous"),urls: (record["urls"] ?? [String]()),types: (record["types"] ?? [Bool]()),commentCount:(record["commentCount"]) ?? 0))
        }
        return posts
    }
    
    //get all posts. belum di sort by date
    func getAllPosts(complete: @escaping (_ instance: [Post], _ error: NSError?) -> ()){
        let pred = NSPredicate(value: true)
        let sort = NSSortDescriptor(key: "creationDate", ascending: false)
        let query = CKQuery(recordType: "Post", predicate: pred)
        query.sortDescriptors = [sort]
        CKContainer.default().publicCloudDatabase.perform(query, inZoneWith: nil) {[self] results, error in
            if let error = error {
                print(error.localizedDescription)
                complete([Post](),error as NSError)
            } else {
                if let checkedResults = results {
                    let posts = self.parsePostResults(records: checkedResults)
                    complete(posts,nil)
                }
            }
        }
    }
    
    func getPostCKRecord(recordName:String,complete: @escaping (_ instance: [CKRecord], _ error: NSError?) -> ()){
        if recordName != ""{
            let pred = NSPredicate(format: "recordID == %@", CKRecord.ID(recordName: recordName))
            let sort = NSSortDescriptor(key: "creationDate", ascending: false)
            let query = CKQuery(recordType: "Post", predicate: pred)
            query.sortDescriptors = [sort]
            CKContainer.default().publicCloudDatabase.perform(query, inZoneWith: nil) { results, error in
                if let error = error {
                    print(error.localizedDescription)
                    complete([CKRecord](),error as NSError)
                } else {
                    if let checkedResults = results {
                        complete(checkedResults,nil)
                    }
                }
            }
        }
    }
    
    //add a post to cloudkit
    func editPost(post:Post){
        DispatchQueue.global().async {
            if let recordName = post.recordName {
                // Fetch Default Container
                let defaultContainer = CKContainer.default()
                // Fetch Private Database
                let publicDatabase = defaultContainer.publicCloudDatabase
                // Fetch User Record
                publicDatabase.fetch(withRecordID: CKRecord.ID(recordName: recordName)) { (record, error) -> Void in
                    if let responseError = error {
                        print(responseError)
                    } else if let postRecord = record{
                        self.editPostBG(originalPost: postRecord,post: post)
                    }
                }
            }
        }
    }
    
    //continuation of addpost()
    private func editPostBG(originalPost:CKRecord,post:Post){
        DispatchQueue.global().async {
            originalPost["location"] = post.location
            originalPost["name"] = post.name
            originalPost["title"] = post.title
            originalPost["postContent"] = post.postContent
            originalPost["dateLastModified"] = Date()
            //originalPost["type"] = post.type
            originalPost["urls"] = post.urls
            originalPost["types"] = post.types
//            originalPost["commentCount"] = post.commentCount
            let publicData = CKContainer.default().publicCloudDatabase
            publicData.save(originalPost) { (record:CKRecord?, error:Error?) in
                if error == nil{
                    print("saved")
                }else{
                    print(error as Any)
                }
            }
        }
    }
    
    //add a post to cloudkit
    func editPostCommentCount(post:Post){
        DispatchQueue.global().async {
            if let recordName = post.recordName {
                // Fetch Default Container
                let defaultContainer = CKContainer.default()
                // Fetch Private Database
                let publicDatabase = defaultContainer.publicCloudDatabase
                // Fetch User Record
                publicDatabase.fetch(withRecordID: CKRecord.ID(recordName: recordName)) { (record, error) -> Void in
                    if let responseError = error {
                        print(responseError)
                    } else if let postRecord = record{
                        self.editPostCommentCountBG(originalPost: postRecord,post: post)
                    }
                }
            }
        }
    }
    
    //continuation of addpost()
    private func editPostCommentCountBG(originalPost:CKRecord,post:Post){
        DispatchQueue.global().async {
            originalPost["commentCount"] = post.commentCount
            let publicData = CKContainer.default().publicCloudDatabase
            publicData.save(originalPost) { (record:CKRecord?, error:Error?) in
                if error == nil{
                    print("saved")
                }else{
                    print(error as Any)
                }
            }
        }
    }
    
    
    func deletePost(post:Post){
        deleteRecordWithID(CKRecord.ID(recordName: post.recordName!)){
            _, _ in
        }
    }
    
    //add a Comment to cloudkit
    func addComment(comment:Comment,post:Post){
        DispatchQueue.global().async {
            
            self.getPostCKRecord(recordName: post.recordName ?? ""){
                posts, error in
                if let checkedPost = posts.first{
                    // Fetch Default Container
                    let defaultContainer = CKContainer.default()
                    // Fetch User Record
                    defaultContainer.fetchUserRecordID { (recordID, error) -> Void in
                        if let responseError = error {
                            print(responseError)
                        } else if let userRecordID = recordID {
                            // Fetch Default Container
                            let defaultContainer = CKContainer.default()
                            // Fetch Private Database
                            let publicDatabase = defaultContainer.publicCloudDatabase
                            // Fetch User Record
                            publicDatabase.fetch(withRecordID: userRecordID) { (record, error) -> Void in
                                if let responseError = error {
                                    print(responseError)
                                } else if let userRecord = record{
                                    self.addCommentBG(user: userRecord,comment:comment,post:checkedPost,postToEdit: post)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    //continuation of addComment()
    private func addCommentBG(user:CKRecord,comment:Comment,post:CKRecord,postToEdit:Post){
        DispatchQueue.global().async {
            let rec = CKRecord(recordType: "Comment")
            rec["commentContent"] = comment.commentContent
            rec["dateCreated"] = comment.dateCreated
            rec["dateLastModified"] = comment.dateLastModified
            rec["owner"] = CKRecord.Reference(record: user, action: .deleteSelf)
            rec["post"] = CKRecord.Reference(record: post, action: .deleteSelf)
            rec["thumbsUp"] = comment.thumbsUp
            rec["thumbsDown"] = comment.thumbsDown
            let publicData = CKContainer.default().publicCloudDatabase
            publicData.save(rec) { (record:CKRecord?, error:Error?) in
                if error == nil{
                    print("saved")
                    var tempPost = Post(location: postToEdit.location, title: postToEdit.title, postContent: postToEdit.postContent, dateCreated: postToEdit.dateCreated, dateLastModified: postToEdit.dateLastModified, name: postToEdit.name, urls: postToEdit.urls, types: postToEdit.types, commentCount:( postToEdit.commentCount + 1 ))
                    tempPost.recordName = postToEdit.recordName
                    self.editPostCommentCount(post: tempPost)
                }else{
                    print(error as Any)
                }
            }
        }
    }
    
    //get all Comment of current post
    func getComment(postRecordName:String,complete: @escaping (_ instance: [Comment], _ error: NSError?) -> ()){
        let reference = CKRecord.Reference(recordID: CKRecord.ID(recordName: postRecordName), action: .deleteSelf)
        let pred = NSPredicate(format: "post == %@", reference)
        let sort = NSSortDescriptor(key: "creationDate", ascending: false)
        let query = CKQuery(recordType: "Comment", predicate: pred)
        query.sortDescriptors = [sort]
        CKContainer.default().publicCloudDatabase.perform(query, inZoneWith: nil) {[self] results, error in
            if let error = error {
                print(error.localizedDescription)
                complete([Comment](),error as NSError)
            } else {
                if let checkedResults = results {
                    let comments = self.parseCommentResults(records: checkedResults)
                    complete(comments,nil)
                }
            }
        }
    }
    
    private func parseCommentResults(records: [CKRecord])->[Comment] {
        var comments = [Comment]()
        
        for record in records {
            comments.append(Comment(recordName: record.recordID.recordName, commentContent: record["commentContent"]!, dateCreated: record["dateCreated"]!, dateLastModified: record["dateLastModified"]!, owner: record["owner"]!, post: record["post"]!, thumbsUp: record["thumbsUp"]!, thumbsDown: record["thumbsDown"]!))
        }
        return comments
    }
    
    //edit a Comment, struct Comment perlu recordname
    //add a Comment to cloudkit
    func editComment(comment:Comment){
        DispatchQueue.global().async {
            if let recordName = comment.recordName {
                // Fetch Default Container
                let defaultContainer = CKContainer.default()
                // Fetch Private Database
                let publicDatabase = defaultContainer.publicCloudDatabase
                // Fetch User Record
                publicDatabase.fetch(withRecordID: CKRecord.ID(recordName: recordName)) { (record, error) -> Void in
                    if let responseError = error {
                        print(responseError)
                    } else if let commentRecord = record{
                        self.editCommentBG(originalComment: commentRecord,comment:comment)
                    }
                }
            }
        }
    }
    
    //continuation of addComment()
    private func editCommentBG(originalComment:CKRecord,comment:Comment){
        DispatchQueue.global().async {
            originalComment["commentContent"] = comment.commentContent
            originalComment["dateLastModified"] = Date()
            originalComment["thumbsUp"] = comment.thumbsUp
            originalComment["thumbsDown"] = comment.thumbsDown
            let publicData = CKContainer.default().publicCloudDatabase
            publicData.save(originalComment) { (record:CKRecord?, error:Error?) in
                if error == nil{
                    print("saved")
                }else{
                    print(error as Any)
                }
            }
        }
    }
    
    func deleteComment(comment:Comment){
        deleteRecordWithID(CKRecord.ID(recordName: comment.recordName!)){
            _, _ in
        }
    }
    
    private func doesUserExist(complete: @escaping (_ instance: Bool) -> ()){
        getUser(){
            user, error in
            if user != nil{
                complete(true)
            }else{
                complete(false)
            }
        }
    }
    
    func initUser(){
        doesUserExist(){
            exist in
            if !exist{
                DispatchQueue.global().async {
                    // Fetch Default Container
                    let defaultContainer = CKContainer.default()
                    // Fetch User Record
                    defaultContainer.fetchUserRecordID { (recordID, error) -> Void in
                        if let responseError = error {
                            print(responseError)
                        } else if let userRecordID = recordID {
                            // Fetch Default Container
                            let defaultContainer = CKContainer.default()
                            // Fetch Private Database
                            let publicDatabase = defaultContainer.publicCloudDatabase
                            // Fetch User Record
                            publicDatabase.fetch(withRecordID: userRecordID) { (record, error) -> Void in
                                if let responseError = error {
                                    print(responseError)
                                } else if let userRecord = record{
                                    self.addUserBG(users: userRecord,user: nil)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func initUser(name:String){
        doesUserExist(){
            exist in
            if !exist{
                DispatchQueue.global().async {
                    // Fetch Default Container
                    let defaultContainer = CKContainer.default()
                    // Fetch User Record
                    defaultContainer.fetchUserRecordID { (recordID, error) -> Void in
                        if let responseError = error {
                            print(responseError)
                        } else if let userRecordID = recordID {
                            // Fetch Default Container
                            let defaultContainer = CKContainer.default()
                            // Fetch Private Database
                            let publicDatabase = defaultContainer.publicCloudDatabase
                            // Fetch User Record
                            publicDatabase.fetch(withRecordID: userRecordID) { (record, error) -> Void in
                                if let responseError = error {
                                    print(responseError)
                                } else if let userRecord = record{
                                    self.addUserBG(users: userRecord,user: User(name: name))
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    //continuation of initUser()
    private func addUserBG(users:CKRecord,user:User?){
        DispatchQueue.global().async {
            let rec = CKRecord(recordType: "User")
            rec["name"] = user?.name
            rec["userRecordName"] = CKRecord.Reference(record: users, action: .deleteSelf)
            let publicData = CKContainer.default().publicCloudDatabase
            publicData.save(rec) { (record:CKRecord?, error:Error?) in
                if error == nil{
                    print("saved")
                }else{
                    print(error as Any)
                }
            }
        }
    }
    
    func getUser(complete: @escaping (_ instance: User?, _ error: NSError?) -> ()){
        getCurrentUserIDAsync() {
            record, error in
            if let id = record{
                let reference = CKRecord.Reference(recordID: id, action: .deleteSelf)
                let pred = NSPredicate(format: "userRecordName == %@", reference)
                let sort = NSSortDescriptor(key: "creationDate", ascending: false)
                let query = CKQuery(recordType: "User", predicate: pred)
                query.sortDescriptors = [sort]
                CKContainer.default().publicCloudDatabase.perform(query, inZoneWith: nil) {[self] results, error in
                    if let error = error {
                        print(error.localizedDescription)
                        complete(nil,error as NSError)
                    } else {
                        if let checkedResults = results {
                            let res = self.parseUserResults(records: checkedResults)
                            complete(res.first,nil)
                        }
                    }
                }
            }
        }
    }
    
    private func parseUserResults(records: [CKRecord])->[User] {
        var list = [User]()
        
        for record in records {
            list.append(User(recordName: record.recordID.recordName, name: record["name"],userRecordName: record["userRecordName"]))
        }
        return list
    }
    
    //edit user, ketika user register atau ganti profil
    func editUser(user:User){
        DispatchQueue.global().async {
            if let recordName = user.recordName {
                // Fetch Default Container
                let defaultContainer = CKContainer.default()
                // Fetch Private Database
                let publicDatabase = defaultContainer.publicCloudDatabase
                // Fetch User Record
                publicDatabase.fetch(withRecordID: CKRecord.ID(recordName: recordName)) { (record, error) -> Void in
                    if let responseError = error {
                        print(responseError)
                    } else if let userRecord = record{
                        self.editUserBG(originalUser: userRecord,user: user)
                    }
                }
            }
        }
    }
    
    //continuation of editUser()
    private func editUserBG(originalUser:CKRecord,user:User?){
        DispatchQueue.global().async {
            originalUser["name"] = user?.name
            let publicData = CKContainer.default().publicCloudDatabase
            publicData.save(originalUser) { (record:CKRecord?, error:Error?) in
                if error == nil{
                    print("saved")
                }else{
                    print(error as Any)
                }
            }
        }
    }
    
    //get user ID of device
    func getCurrentUserIDAsync(complete: @escaping (_ instance: CKRecord.ID?, _ error: NSError?) -> ()) {
        let container = CKContainer.default()
        container.fetchUserRecordID() {
            record, error in
            if error != nil {
                print(error!.localizedDescription)
                complete(nil, error as NSError?)
            } else {
                complete(record, nil)
            }
        }
    }
    
    private func deleteRecordWithID(_ recordID: CKRecord.ID, completion: ((_ recordID: CKRecord.ID?, _ error: Error?) -> Void)?) {
        // Fetch Default Container
        let defaultContainer = CKContainer.default()
        let publicDatabase = defaultContainer.publicCloudDatabase
        publicDatabase.delete(withRecordID: recordID) { (recordID, error) in
            completion?(recordID, error)
        }
    }
    
    private func parseArticleResults(records: [CKRecord])->[Article] {
        var article = [Article]()
        
        for record in records {
            article.append(Article(recordName: record.recordID.recordName, title: record["title"]!, love: (record["love"] ?? 0), urls: (record["urls"] ?? [String]()), articleContent: record["articleContent"]!))
        }
        return article
    }
    
    //get all posts. belum di sort by date
    func getAllArticles(complete: @escaping (_ instance: [Article], _ error: NSError?) -> ()){
        let pred = NSPredicate(value: true)
        //let sort = NSSortDescriptor(key: "creationDate", ascending: false)
        let query = CKQuery(recordType: "Article", predicate: pred)
        //query.sortDescriptors = [sort]
        CKContainer.default().publicCloudDatabase.perform(query, inZoneWith: nil) {[self] results, error in
            if let error = error {
                print(error.localizedDescription)
                complete([Article](),error as NSError)
            } else {
                if let checkedResults = results {
                    let articles = self.parseArticleResults(records: checkedResults)
                    complete(articles,nil)
                }
            }
        }
    }
    
    func getAllPostWithComments(complete: @escaping (_ instance: [PostAndComments], _ error: NSError?) -> ()){
        getAllPosts() {
            posts, error in
            var postAndComment:[PostAndComments] = [PostAndComments]()
            for post in posts{
                postAndComment.append(PostAndComments(post: post))
            }
            self.getAllPostWithCommentsBG(postAndComments: postAndComment){
                returnedPostWithComments, error in
                complete(returnedPostWithComments,error)
            }
        }
    }
    
    private func getAllPostWithCommentsBG(postAndComments:[PostAndComments],complete: @escaping (_ instance: [PostAndComments], _ error: NSError?) -> ()){
        var tempPostAndComments = postAndComments
        
        for (index,postAndComment) in tempPostAndComments.enumerated(){
            var locked = true
            self.getComment(postRecordName: postAndComment.post.recordName ?? ""){
                comments, error in
                for comment in comments{
                    tempPostAndComments[index].comments.append(comment)
                }
                locked = false
            }
            while(locked){sleep(1)}
        }
        complete(tempPostAndComments, nil)
    }
}

struct Post{
    var location:CLLocation
    var recordName:String?
    var title:String
    var name:String?
    var postContent:String
    var dateCreated:Date
    var dateLastModified:Date
    //var type:String
    var types:[Bool]
    var owner:CKRecord.Reference?
    var urls:[String]
    var commentCount:Int
    init(recordName:String?,location:CLLocation, title:String, postContent:String, dateCreated:Date, dateLastModified:Date, owner:CKRecord.Reference?, name:String?, urls:[String], types:[Bool],commentCount:Int) {
        self.recordName = recordName
        self.location = location
        self.title = title
        self.postContent = postContent
        self.dateCreated = dateCreated
        self.dateLastModified = dateLastModified
        //self.type = type
        self.owner = owner
        self.name = name
        self.urls = urls
        self.types = types
        self.commentCount = commentCount
    }
    init(location:CLLocation, title:String, postContent:String, dateCreated:Date, dateLastModified:Date, name:String?, urls:[String], types:[Bool],commentCount:Int) {
        self.location = location
        self.title = title
        self.postContent = postContent
        self.dateCreated = dateCreated
        self.dateLastModified = dateLastModified
        //self.type = type
        self.name = name
        self.urls = urls
        self.types = types
        self.commentCount = commentCount
    }
}

struct Comment{
    var recordName:String?
    var commentContent:String
    var dateCreated:Date
    var dateLastModified:Date
    var owner:CKRecord.Reference?
    var post:CKRecord.Reference?
    var thumbsUp:Int
    var thumbsDown:Int
    init(recordName:String?, commentContent:String, dateCreated:Date, dateLastModified:Date, owner:CKRecord.Reference?, post:CKRecord.Reference?, thumbsUp:Int, thumbsDown:Int) {
        self.recordName = recordName
        self.commentContent = commentContent
        self.dateCreated = dateCreated
        self.dateLastModified = dateLastModified
        self.owner = owner
        self.post = post
        self.thumbsUp = thumbsUp
        self.thumbsDown = thumbsDown
    }
    init(commentContent:String, dateCreated:Date, dateLastModified:Date, post:CKRecord.Reference?, thumbsUp:Int, thumbsDown:Int) {
        self.commentContent = commentContent
        self.dateCreated = dateCreated
        self.dateLastModified = dateLastModified
        self.post = post
        self.thumbsUp = thumbsUp
        self.thumbsDown = thumbsDown
    }
}

struct User{
    var recordName:String?
    var userRecordName:CKRecord.Reference?
    var name:String?
    init(recordName:String?,name:String?,userRecordName:CKRecord.Reference?) {
        self.name = name
        self.recordName = recordName
        self.userRecordName = userRecordName
    }
    init(recordName:String?,name:String?) {
        self.name = name
        self.recordName = recordName
    }
    init(name:String?) {
        self.name = name
    }
    init(){
        
    }
}

struct Article{
    var recordName:String?
    var title:String
    var love:Int
    var urls:[String]
    var articleContent:String
    init(recordName:String?,title:String,love:Int,urls:[String],articleContent:String){
        self.recordName = recordName
        self.title = title
        self.love = love
        self.urls = urls
        self.articleContent = articleContent
    }
}

struct PostAndComments{
    var post:Post
    var comments:[Comment]
    init(post:Post,comments:[Comment]) {
        self.post = post
        self.comments = comments
    }
    init(post:Post) {
        self.post = post
        self.comments = [Comment]()
    }
}
