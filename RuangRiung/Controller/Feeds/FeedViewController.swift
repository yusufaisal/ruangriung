//
//  FeedViewController.swift
//  RuangRiung
//
//  Created by iSal on 17/08/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit
import MapKit

class FeedViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    var refresher:UIRefreshControl!
    var feeds:[Post] = [Post]()
    let APIHelper = CloudKitHelper()
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView!.alwaysBounceVertical = true
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = UIColor.red
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.tableView!.addSubview(refresher)
        
        self.getFeeds()
        
    }

    
    private func getFeeds(){
        APIHelper.getAllPosts(complete: { (res, _) in
            DispatchQueue.main.async {
                self.feeds = res
                self.tableView.reloadData()
            }
        })
    }
    
    @objc func loadData() {
        getFeeds()
        self.refresher.endRefreshing()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feeds.count
    }
    
    @objc func goToDetail(_ sender: UITapGestureRecognizer){
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PostView
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.username.text = feeds[indexPath.row].name
        cell.postContent.text = feeds[indexPath.row].postContent
        cell.title.text = feeds[indexPath.row].title
        cell.numComment.text = String(feeds[indexPath.row].commentCount)
        cell.date.text = timeAgoSinceDate(feeds[indexPath.row].dateCreated,currentDate: Date(), numericDates: true)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "feeddetail", sender: self.feeds[indexPath.row])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! DetailFeedController
        let post = sender as! Post

        vc.posts = post
    }
}

class PostView: UITableViewCell {
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var card: UIView!
    @IBOutlet weak var postContent: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var numComment: UILabel!
    @IBOutlet weak var date: UILabel!
}
