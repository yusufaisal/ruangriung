//
//  HeaderCell.swift
//  RuangRiung
//
//  Created by iSal on 23/08/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {

    
    static let identifier: String = "HeaderCell"
   
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var poi: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var mainVIew: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func setupMainView(isPhotoAvailable: Bool){
        if !isPhotoAvailable {
            let mainConstraints:[NSLayoutConstraint] = [
                mainVIew.heightAnchor.constraint(equalToConstant: 127 )
            ]
            NSLayoutConstraint.activate(mainConstraints)
        } else {
            
            let mainConstraints:[NSLayoutConstraint] = [
                mainVIew.heightAnchor.constraint(equalToConstant: 229 )
            ]
            NSLayoutConstraint.activate(mainConstraints)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
