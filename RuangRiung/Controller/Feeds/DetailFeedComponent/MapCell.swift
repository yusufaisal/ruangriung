//
//  MapCell.swift
//  RuangRiung
//
//  Created by iSal on 23/08/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit
import MapKit

class MapCell: UITableViewCell, MKMapViewDelegate  {

    static let identifier: String  = "MapCell"
    @IBOutlet weak var mapview: MKMapView!
    let locationManager = CLLocationManager()
    let regionInMeters: Double = 1000
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.mapview.delegate = self
        self.mapview.showsUserLocation = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func centerViewLocation(location: CLLocationCoordinate2D){
        let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
        mapview.setRegion(region, animated: true)
        setupAnnotation(location: location)
    }
    
    func setupAnnotation(location:CLLocationCoordinate2D){
        let location = location
        let pin = CustomPin(pinTitle: "", pinSubTitle: "", location: location, isVictim: true, date: nil, type: nil, name:nil)
        self.mapview.addAnnotation(pin)
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = NSStringFromClass(CustomPin.self)
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier )
        
        let rightButton = UIButton(type: .infoDark)
        annotationView?.rightCalloutAccessoryView = rightButton
        if let annotation = annotation as? CustomPin {
            
            var img:UIImage!
            if annotation.isVictim {
                img = UIImage(named: "pin1")
            } else if !annotation.isVictim {
                img = UIImage(named: "pin2")
            }
            annotationView?.image = img
            let offset = CGPoint(x: img.size.width / 2, y: -(img.size.height / 2) )
            annotationView?.centerOffset = offset
        }
        else if annotation === mapView.userLocation{
            annotationView?.image = UIImage(named: "point")
            print("pin user")
        }
        annotationView?.canShowCallout = true
        return annotationView
    }
}
