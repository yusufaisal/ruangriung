//
//  MapViewController.swift
//  RuangRiung
//
//  Created by iSal on 15/08/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit
import MapKit
import SwiftyDropbox

class CustomPin: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle:String?
    var isVictim: Bool!
    var date: Date?
    var type: String?
    var name: String?
    
    init(pinTitle: String, pinSubTitle: String, location: CLLocationCoordinate2D,isVictim: Bool,date: Date?, type: String?, name: String?) {
        self.coordinate = location
        self.title = pinTitle
        self.subtitle = pinSubTitle
        self.isVictim = isVictim
        self.date = date
        self.type = type
        self.name = name
        
    }
}

class MapViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate,UITableViewDataSource, MKMapViewDelegate {
    @IBOutlet weak var Lokasilabel: UILabel!
    var matchingItems: [MKMapItem] = []
    var storiesAnnotation: [MKAnnotation] = [MKAnnotation]()
    var selectedPin: MKPlacemark?
    var resultSearchControllerrS: UISearchController!
    @IBOutlet weak var tableViewSeacrh: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var centerButton: UIButton!
    @IBOutlet weak var topLocationInfo: UIView!
    let locationManager = CLLocationManager()
    let regionInMeters: Double = 1000
    let client = DropboxClient(accessToken: DropboxAPI_AccessToken)
    var posts:[Post] = [Post]()
    let ck:CloudKitHelper = CloudKitHelper()
    var lastView:CLLocationCoordinate2D?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {return .default}
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
        mapView.delegate = self
        tableViewSeacrh.delegate = self
        tableViewSeacrh.dataSource = self
        searchBar.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
        Lokasilabel.text = mapView.userLocation.title
        centerButton.layer.borderWidth = 1
        centerButton.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        topLocationInfo.layer.shadowColor = UIColor.gray.cgColor
        topLocationInfo.layer.shadowOpacity = 0.8
        topLocationInfo.layer.shadowOffset = CGSize.zero
        topLocationInfo.layer.shadowRadius = 5
        registerAnnotationViews()
        checkLocationServices()
        setupPin()
        ck.initUser()
    }
    
    @IBAction func addPostButtonClicked(_ sender: Any) {
        _ = UIStoryboardSegue(identifier: "toAddPostView", source: self, destination: AddStoryTableViewController.init())
    }
    
    private func registerAnnotationViews() {
        mapView.register(MKAnnotationView.self, forAnnotationViewWithReuseIdentifier: NSStringFromClass(CustomPin.self))
    }
    
   
    @IBAction func centerTheView(_ sender: Any) {
        centerViewLocation()
    }
    
    func setupPin(){
        ck.getAllPosts(){resultPosts, error in
            self.storiesAnnotation = [MKAnnotation]()
            self.posts = resultPosts
                for post in resultPosts{
                    let location = post.location.coordinate
                    let pin = CustomPin(pinTitle: post.name!, pinSubTitle: "", location: location, isVictim: true, date: post.dateCreated , type: "", name: post.title) // todo type dari bool
                    self.storiesAnnotation.append(pin)
                    self.mapView.addAnnotation(pin)
                }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        mapView.removeAnnotations(storiesAnnotation)
        setupPin()
    }
    
    
    func setupLoactionManager(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }

    func checkLocationServices(){
        if CLLocationManager.locationServicesEnabled(){
            // setup our location manager
            setupLoactionManager()
            checkLocationAuthorization()
        } else {
            print("is disabled")
        }
    }
    
    func centerViewLocation(){
        if let location = locationManager.location?.coordinate{
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        tableViewSeacrh.isHidden = true
        self.view.endEditing(true)
        searchBar.text = ""
    }
    
    var tempName: String = ""
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableViewSeacrh.isHidden = true
        searchBar.text = ""
        self.searchBar.endEditing(true)
        let placemark = matchingItems[indexPath.row].placemark
        removeSpecificAnnotation(nama: tempName)
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let region = MKCoordinateRegion(center: placemark.coordinate , span: span)
        let pin = CustomPin(pinTitle: placemark.name!, pinSubTitle: "", location: placemark.coordinate, isVictim: false, date: nil, type: nil,name:nil)
        mapView.addAnnotation(pin)
        mapView.setRegion(region, animated: true)
        tempName = placemark.name!
    }
    
    func removeSpecificAnnotation(nama: String) {
        for annotation in self.mapView.annotations {
            if let title = annotation.title, title == nama {
                self.mapView.removeAnnotation(annotation)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return matchingItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UISeacrhTableViewCell
        let selectedItem = matchingItems[indexPath.row].placemark
        
        cell.titleLabel?.text = selectedItem.name
        cell.subtiitleLabel?.text = parseAddress(selectedItem: selectedItem)
        return cell
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
         tableViewSeacrh.isHidden = false
        guard let mapView = mapView,
        let searchBarText = searchBar.text else { return }
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchBarText
        request.region = mapView.region
        let search = MKLocalSearch(request: request)
        
        search.start { response, _ in
            guard let response = response else {
                return
            }
            self.matchingItems = response.mapItems
            self.tableViewSeacrh.reloadData()
        }
    }
    
    func getLocationUser(){
        let currentLoc = locationManager.location?.coordinate
        let userLoc = CLLocation(latitude: currentLoc!.latitude, longitude: currentLoc!.longitude)
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(userLoc, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            if let street = placeMark.thoroughfare {
                self.Lokasilabel.text = street ?? "My Location"
                print("User Location: "+street)
            }
           
        })

        
    }
    
    func checkLocationAuthorization(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            mapView.showsUserLocation = true
            mapView.showsCompass = true
            mapView.showsPointsOfInterest = true
            locationManager.startUpdatingLocation()
            centerViewLocation()
            
        case .authorizedAlways:
            mapView.showsUserLocation = true
            mapView.showsCompass = true
            mapView.showsPointsOfInterest = true
            locationManager.startUpdatingLocation()
            centerViewLocation()
        case .denied:
            break
        case .restricted:
            break
        case .notDetermined:
            locationManager.requestAlwaysAuthorization()
        @unknown default:
            fatalError()
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = NSStringFromClass(CustomPin.self)
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier )
        
        if let annotation = annotation as? CustomPin {
            var imagePoint:UIImage!
            if annotation.isVictim {
                imagePoint = UIImage(named: "pin1")
                configureDetailView(annotationView: annotationView!, annotation: annotation)

            } else if !annotation.isVictim {
                imagePoint = UIImage(named: "pin2")
            }
            annotationView?.image = imagePoint
            annotationView?.centerOffset = CGPoint(x: imagePoint.size.width / 2, y: -(imagePoint.size.height / 2) )
            
            
        } else if annotation === mapView.userLocation{
            annotationView?.image = UIImage(named: "point")
        }
        annotationView?.canShowCallout = true
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        let region = MKCoordinateRegion.init(center: lastView!, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
        mapView.setRegion(region, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        lastView = mapView.centerCoordinate
        for v in view.subviews{
            let colloutView = v.subviews[0]
            colloutView.backgroundColor = #colorLiteral(red: 0.8901960784, green: 0.3607843137, blue: 0.3529411765, alpha: 0.9)
            if v.subviews.count > 0 {
                if colloutView.subviews.count > 0 {
                    if colloutView.subviews[0].subviews.count > 0{
                        colloutView.subviews[0].subviews.forEach { (view) in
                            if let label = view as? UILabel{
                                label.textColor = UIColor.white
                            }
                        }
                    }
                }
            }
        }
        let region = MKCoordinateRegion.init(center: view.annotation!.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)
        mapView.setRegion(region, animated: true)
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer){
        print("is tapped")
        print(sender)
        performSegue(withIdentifier: "goToDetailStory", sender: self.posts[0])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(segue.destination)
        if  let destName = segue.destination as? UINavigationController {
            if let target = destName.topViewController as? DetailFeedController {
                 target.posts = sender as? Post
            }
           
        }
    }
    
    func configureDetailView(annotationView: MKAnnotationView, annotation: CustomPin) { 
        let spacing:CGFloat = 10
        let margin:CGFloat = 0
        
        let calloutView = UIView()
        
        let views = ["snapshotView": calloutView]
        calloutView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[snapshotView(300)]", options: [], metrics: nil, views: views))
        calloutView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[snapshotView(100)]", options: [], metrics: nil, views: views))
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTap))
        calloutView.addGestureRecognizer(tapGesture)
        
        // Define view components
        let title:UILabel = {
            let label = UILabel()
            label.text = annotation.name
            label.textColor = .white
            label.font = UIFont.systemFont(ofSize: 24, weight: .semibold)
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        let type:UILabel = {
            let label = UILabel()
            label.text = annotation.type
            label.textColor = .white
            label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        let date:UILabel = {
            let label = UILabel()
            label.text = dateToString(date: annotation.date!)
            label.textColor = .white
            label.font = UIFont.systemFont(ofSize: 14)
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        
        // Add components as a subview into calloutView
        calloutView.addSubview(title)
        calloutView.addSubview(type)
        calloutView.addSubview(date)
        
        // Setup constraints for each component
        let titleConstraints:[NSLayoutConstraint] = [
            title.topAnchor.constraint(equalTo: calloutView.topAnchor, constant: 0),
            title.leftAnchor.constraint(equalTo: calloutView.leftAnchor, constant: margin),
            title.rightAnchor.constraint(equalTo: calloutView.rightAnchor, constant: margin),
            
            ]
        let typeConstraints:[NSLayoutConstraint] = [
            type.topAnchor.constraint(equalTo: title.bottomAnchor, constant: spacing),
            type.leftAnchor.constraint(equalTo: calloutView.leftAnchor, constant: margin),
            type.rightAnchor.constraint(equalTo: calloutView.rightAnchor, constant: margin),
        ]
        let dateConstraints:[NSLayoutConstraint] = [
            date.topAnchor.constraint(equalTo: type.bottomAnchor, constant: spacing),
            date.leftAnchor.constraint(equalTo: calloutView.leftAnchor, constant: margin),
            date.rightAnchor.constraint(equalTo: calloutView.rightAnchor, constant: margin),
        ]
        
        // Activate Constraints
        NSLayoutConstraint.activate(titleConstraints)
        NSLayoutConstraint.activate(typeConstraints)
        NSLayoutConstraint.activate(dateConstraints)

        
        annotationView.detailCalloutAccessoryView = calloutView
    }
}







