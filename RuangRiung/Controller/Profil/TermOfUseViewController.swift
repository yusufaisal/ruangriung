//
//  TermOfUseViewController.swift
//  RuangRiung
//
//  Created by Louis  Valen on 27/08/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit

class TermOfUseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.navigationController?.navigationBar.backItem?.title = "Profil"
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.9376266599, green: 0.3580080867, blue: 0.360183239, alpha: 1)

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
