//
//  ArticleDetailViewController.swift
//  RuangRiung
//
//  Created by michael gunawan on 25/08/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit

class ArticleDetailViewController: UIViewController {
    var article:Article?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var titleNavbar: UINavigationItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.contentSize = CGSize(width: CGFloat(self.view.frame.width), height: CGFloat(self.view.frame.height+100))
        loadArticle()
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.937254902, green: 0.3568627451, blue: 0.3607843137, alpha: 1)
        // Do any additional setup after loading the view.
    }
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    func loadArticle(){
        self.navigationItem.title = article?.title ?? ""
        if((article?.urls.count ?? 0) > 0){
            self.imageView.downloaded(from: (article?.urls.first)!)
        }
        
        textView.attributedText = parseHtmlToString(text: (article?.articleContent ?? ""))
        adjustUITextViewHeight(arg: textView)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
