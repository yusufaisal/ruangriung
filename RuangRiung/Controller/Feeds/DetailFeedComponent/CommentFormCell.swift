//
//  CommentFormCell.swift
//  RuangRiung
//
//  Created by iSal on 23/08/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit

class CommentFormCell: UITableViewCell, UITextFieldDelegate {

    static let identifier:String  = "CommentFormCell"
    @IBOutlet weak var commentText: UITextField!
    @IBOutlet weak var numOfComment: UILabel!
    var delegate:DetailFeedProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commentText.delegate = self
        // Initialization code
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        self.delegate?.changeComment(newComment: commentText.text!)
//        return true
//    }
//    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        self.delegate?.changeComment(newComment: commentText.text!)
//    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        DispatchQueue.main.async {
            self.delegate?.changeComment(newComment: self.commentText.text!)
            self.delegate?.addComment()
            self.commentText.text = ""
            self.commentText.resignFirstResponder()

        }
        return true
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
