//
//  ArticlesViewController.swift
//  RuangRiung
//
//  Created by iSal on 26/08/19.
//  Copyright © 2019 iSal. All rights reserved.
//

import UIKit

class ArticlesCell:UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var numOfLove: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var card: UIView!
}

class ArticlesViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    let refresher = UIRefreshControl()
    let APIHelper = CloudKitHelper()
    var articles:[Article] = [Article]()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView!.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.red
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.collectionView!.addSubview(refresher)
        getArticles()
    }
    
    @objc func loadData() {
        getArticles()
        self.refresher.endRefreshing()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    private func getArticles(){
        
        APIHelper.getAllArticles { (articles, err) in
            if let e = err {
                print(e)
            } else {
                DispatchQueue.main.async{
                    self.articles = articles
                    self.collectionView.reloadData()
                    print(self.articles.count)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return articles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellArticle", for: indexPath) as! ArticlesCell
        
        cell.title.text = articles[indexPath.row].title
        //cell.content.text = articles[indexPath.row].articleContent
        cell.content.attributedText = parseHtmlToString(text: articles[indexPath.row].articleContent)
        cell.numOfLove.text = String(articles[indexPath.row].love)
        // cell.date.text = dateFormater(date: articles[indexPath.row].)
        cell.card.layer.shadowColor = UIColor.gray.cgColor
        cell.card.layer.shadowOpacity = 0.3
        cell.card.layer.shadowOffset = CGSize.zero
        cell.card.layer.shadowRadius = 3

        let pictureURL = URL(string:articles[indexPath.row].urls.first!)!
        let session = URLSession(configuration: .default)
        
        let downloadPicTask = session.dataTask(with: pictureURL) { (data, response, error) in
            if let e = error {
                print("Error downloading cat picture: \(e)")
            } else {
                if let res = response as? HTTPURLResponse {
                    print("Downloaded cat picture with response \(res.statusCode)")
                    if let imageData = data {
                        cell.image.image =  UIImage(data: imageData)
                        
                    } else {
                        print("Couldn't get image: Image is nil")
                    }
                } else {
                    print("Couldn't get response code for some reason")
                }
            }
        }
        
        downloadPicTask.resume()
        
        return cell
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! ArticleDetailViewController
        let cell = sender as! ArticlesCell
        let indexPath = self.collectionView!.indexPath(for: cell)
        
        vc.article = articles[indexPath!.row]
    }

}
